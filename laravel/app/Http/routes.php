<?php

Route::group(['middleware' => ['web']], function () {
    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::post('parceiros/imagemUpload', 'ParceirosController@imagemUpload')->name('painel.parceiros.imagem');
        Route::resource('parceiros', 'ParceirosController');
        Route::post('links/imagemUpload', 'LinksController@imagemUpload')->name('painel.links.imagem');
        Route::resource('links', 'LinksController');
        Route::post('artigos/imagemUpload', 'ArtigosController@imagemUpload')->name('painel.artigos.imagem');
		Route::resource('artigos', 'ArtigosController');
		Route::resource('paginas', 'PaginasController');
		Route::resource('chamada-home', 'ChamadaHomeController', ['only' => ['index', 'update']]);
		Route::resource('citacoes', 'CitacoesController');
		Route::resource('banners', 'BannersController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });

    // Front-end
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('artigos', 'ArtigosController@index')->name('artigos');
    Route::get('links', 'LinksController@index')->name('links');
    Route::get('parceiros', 'ParceirosController@index')->name('parceiros');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::get('{pagina_slug}', 'PaginasController@index')->name('pagina');
});
