<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ChamadaHome;
use App\Models\Citacao;
use App\Models\Banner;

class HomeController extends Controller
{
    public function index()
    {
        $chamada  = ChamadaHome::first();
        $citacoes = Citacao::ordenados()->get();
        $banners  = Banner::ordenados()->get();

        return view('frontend.home', compact('chamada', 'citacoes', 'banners'));
    }
}
