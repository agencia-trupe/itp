<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ChamadaHomeRequest;
use App\Http\Controllers\Controller;

use App\Models\ChamadaHome;

class ChamadaHomeController extends Controller
{
    public function index()
    {
        $registro = ChamadaHome::first();

        return view('painel.chamada-home.edit', compact('registro'));
    }

    public function update(ChamadaHomeRequest $request, ChamadaHome $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.chamada-home.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
