<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ParceirosRequest;
use App\Http\Requests\ParceirosImagemRequest;
use App\Http\Controllers\Controller;

use App\Models\Parceiro;
use App\Models\ParceirosImagem;

class ParceirosController extends Controller
{
    public function index()
    {
        $registros = Parceiro::ordenados()->get();
        $parceirosImagem = ParceirosImagem::first();

        return view('painel.parceiros.index', compact('registros', 'parceirosImagem'));
    }

    public function create()
    {
        return view('painel.parceiros.create');
    }

    public function store(ParceirosRequest $request)
    {
        try {

            $input = $request->all();

            Parceiro::create($input);
            return redirect()->route('painel.parceiros.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Parceiro $registro)
    {
        return view('painel.parceiros.edit', compact('registro'));
    }

    public function update(ParceirosRequest $request, Parceiro $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);
            return redirect()->route('painel.parceiros.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Parceiro $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.parceiros.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function imagemUpload(ParceirosImagemRequest $request)
    {
        try {

            ParceirosImagem::first()->update([
                'imagem' => ParceirosImagem::uploadImagem()
            ]);

            return back()->with('success', 'Imagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar imagem: '.$e->getMessage()]);

        }
    }
}
