<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ArtigosRequest;
use App\Http\Requests\ArtigosImagemRequest;
use App\Http\Controllers\Controller;

use App\Models\Artigo;
use App\Models\ArtigosImagem;

class ArtigosController extends Controller
{
    public function index()
    {
        $registros = Artigo::ordenados()->get();
        $artigosImagem = ArtigosImagem::first();

        return view('painel.artigos.index', compact('registros', 'artigosImagem'));
    }

    public function create()
    {
        return view('painel.artigos.create');
    }

    public function store(ArtigosRequest $request)
    {
        try {

            $input = $request->all();
            if ($request->hasFile('arquivo')) $input['arquivo'] = Artigo::uploadArquivo();

            Artigo::create($input);
            return redirect()->route('painel.artigos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Artigo $registro)
    {
        return view('painel.artigos.edit', compact('registro'));
    }

    public function update(ArtigosRequest $request, Artigo $registro)
    {
        try {

            $input = $request->all();
            if ($request->hasFile('arquivo')) $input['arquivo'] = Artigo::uploadArquivo();

            $registro->update($input);
            return redirect()->route('painel.artigos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Artigo $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.artigos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function imagemUpload(ArtigosImagemRequest $request)
    {
        try {

            ArtigosImagem::first()->update([
                'imagem' => ArtigosImagem::uploadImagem()
            ]);

            return back()->with('success', 'Imagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar imagem: '.$e->getMessage()]);

        }
    }
}
