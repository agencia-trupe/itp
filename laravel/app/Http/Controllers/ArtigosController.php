<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Artigo;
use App\Models\ArtigosImagem;

class ArtigosController extends Controller
{
    public function index()
    {
        $artigos = Artigo::ordenados()->paginate(12);
        $artigosImagem = ArtigosImagem::first();

        return view('frontend.artigos', compact('artigos', 'artigosImagem'));
    }
}
