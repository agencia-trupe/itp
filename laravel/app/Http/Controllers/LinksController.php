<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Link;
use App\Models\LinksImagem;

class LinksController extends Controller
{
    public function index()
    {
        $links = Link::ordenados()->get();
        $linksImagem = LinksImagem::first();

        return view('frontend.links', compact('links', 'linksImagem'));
    }
}
