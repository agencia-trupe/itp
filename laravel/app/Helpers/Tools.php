<?php

namespace App\Helpers;

class Tools
{

    public static function telefones($string)
    {
        return array_map(function($telefone) {
            return trim($telefone);
        }, explode(',', $string));
    }

}
