<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Citacao extends Model
{
    protected $table = 'citacoes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            'width'  => 100,
            'height' => 100,
            'path'   => 'assets/img/citacoes/'
        ]);
    }
}
