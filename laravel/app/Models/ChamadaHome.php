<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChamadaHome extends Model
{
    protected $table = 'chamada_home';

    protected $guarded = ['id'];
}
