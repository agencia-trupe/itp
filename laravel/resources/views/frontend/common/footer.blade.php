    <footer>
        <div class="center">
            <div class="col">
                <p class="marca">{{ config('site.name') }}</p>
                <p class="telefones">
                    @foreach(Tools::telefones($contato->telefones) as $telefone)
                    <span>{{ $telefone }}</span>
                    @endforeach
                </p>
            </div>

            <div class="col">
                <div class="endereco">
                    {!! $contato->endereco !!}
                </div>
            </div>

            <div class="col">
                <a href="{{ route('contato') }}" class="contato">FALE CONOSCO</a>
            </div>

            <p class="copyright">
                © {{ date('Y') }} {{ config('site.name') }} - Todos os direitos reservados.
                <span></span>
                <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
