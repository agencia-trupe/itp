    <header>
        <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>

        <nav>
            @foreach($paginas as $pagina)
            <a href="{{ route('pagina', $pagina->slug) }}" @if(Route::currentRouteName() === 'pagina' && isset(Route::current()->parameter('pagina_slug')->slug) && Route::current()->parameter('pagina_slug')->slug == $pagina->slug) class="active" @endif>{{ $pagina->titulo }}</a>
            @endforeach
            <a href="{{ route('artigos') }}" @if(Route::currentRouteName() === 'artigos') class="active" @endif>Artigos</a>
            <a href="{{ route('links') }}" @if(Route::currentRouteName() === 'links') class="active" @endif>Links</a>
            <a href="{{ route('parceiros') }}" @if(Route::currentRouteName() === 'parceiros') class="active" @endif>Parceiros</a>
            <a href="{{ route('contato') }}" @if(Route::currentRouteName() === 'contato') class="active" @endif>Contato</a>
        </nav>

        <p class="telefones">
            @foreach(Tools::telefones($contato->telefones) as $telefone)
            <span>{{ $telefone }}</span>
            @endforeach
        </p>

        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
    </header>
