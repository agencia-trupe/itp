@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
            <div class="slide" style="background-image:url('{{ asset('assets/img/banners/'.$banner->imagem) }}')"></div>
            @endforeach
        </div>

        <div class="citacoes">
            @foreach($citacoes as $citacao)
            <div class="slide">
                <img src="{{ asset('assets/img/citacoes/'.$citacao->imagem) }}" alt="">
                <p>
                    {{ $citacao->frase }}
                    <span>{{ $citacao->autor }}</span>
                </p>
            </div>
            @endforeach
        </div>

        <div class="textos">
            <div class="left">
                <h3>INSTITUTO TRIANON DE PSICANÁLISE</h3>
                <h2>{{ $chamada->titulo }}</h2>
                <div>{!! $chamada->texto !!}</div>
            </div>
            <div class="right">
                <h2>PROJETO ESCUTA</h2>
                <p>A Psicanálise Aplicada à Terapêutica</p>
                <a href="{{ route('contato') }}">Agende a sua consulta</a>
            </div>
        </div>
    </div>

@endsection
