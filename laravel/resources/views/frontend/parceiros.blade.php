@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="imagem" style="background-image:url('{{ asset('assets/img/parceiros/'.$parceirosImagem->imagem) }}')"></div>

        <div class="texto">
            <h1>PARCEIROS</h1>

            <div class="parceiros">
            @foreach($parceiros as $parceiro)
                @if($parceiro->link)
                <a href="{{ $parceiro->link }}" class="parceiro" target="_blank">
                    {{ $parceiro->nome }}
                </a>
                @else
                <div class="parceiro">
                    {{ $parceiro->nome }}
                </div>
                @endif
            @endforeach
            </div>
        </div>
    </div>

@endsection
