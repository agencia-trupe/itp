@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="imagem" style="background-image:url('{{ asset('assets/img/links/'.$linksImagem->imagem) }}')"></div>

        <div class="texto">
            <h1>LINKS</h1>

            <div class="links">
            @foreach($links as $link)
                <a href="{{ $link->link }}" target="_blank">
                    {{ $link->titulo }}
                </a>
            @endforeach
            </div>
        </div>
    </div>

@endsection
