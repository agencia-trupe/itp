@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Artigos
            <a href="{{ route('painel.artigos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Artigo</a>
        </h2>
    </legend>

    <div class="row">
        <div class="col-md-9">
            @if(!count($registros))
            <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
            @else
            <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="artigos">
                <thead>
                    <tr>
                        <th>Ordenar</th>
                        <th>Título</th>
                        <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($registros as $registro)
                    <tr class="tr-row" id="{{ $registro->id }}">
                        <td>
                            <a href="#" class="btn btn-info btn-sm btn-move">
                                <span class="glyphicon glyphicon-move"></span>
                            </a>
                        </td>
                        <td>{{ $registro->titulo }}</td>
                        <td class="crud-actions">
                            {!! Form::open([
                                'route'  => ['painel.artigos.destroy', $registro->id],
                                'method' => 'delete'
                            ]) !!}

                            <div class="btn-group btn-group-sm">
                                <a href="{{ route('painel.artigos.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                                    <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                                </a>

                                <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                            </div>

                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endif
        </div>
        <div class="col-md-3">
            <div class="well form-group">
                {!! Form::open(['route' => 'painel.artigos.imagem', 'files' => true]) !!}
                    {!! Form::label('imagem', 'Imagem') !!}
                    <img src="{{ url('assets/img/artigos/'.$artigosImagem->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
                    {!! Form::file('imagem', ['class' => 'form-control']) !!}
                    {!! Form::submit('Alterar', ['class' => 'btn btn-sm btn-success', 'style' => 'margin-top:10px']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
