@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Páginas /</small> Editar Página</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.paginas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.paginas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
