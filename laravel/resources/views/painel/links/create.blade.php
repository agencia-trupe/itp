@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Links /</small> Adicionar Link</h2>
    </legend>

    {!! Form::open(['route' => 'painel.links.store', 'files' => true]) !!}

        @include('painel.links.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
