<?php

return [

    'name'        => 'Instituto Trianon de Psicanálise',
    'title'       => 'Instituto Trianon de Psicanálise',
    'description' => '',
    'keywords'    => 'psicanalista, psicanalise, PROJETO ESCUTA, tratamento psicanalítico, operação analítica, analista, psicanalise terapeutica, Centro Psicanalítico, Conversação Clínica',
    'share_image' => '',
    'analytics'   => null

];
